class CreadorP {
// metodo de una clase            
    static render(elSelector, i, nombre, direccion, barrio, telefono, id_garage){        
        document.querySelector(elSelector).innerHTML += CreadorP.crearGarageDiv(i, nombre, direccion, barrio, telefono, id_garage);
    }
    
    static renderCliente(elSelector, i, nombre, direccion, barrio, telefono, id_garage){
        
        document.querySelector(elSelector).innerHTML += CreadorP.crearGarageDivCliente(i, nombre, direccion, barrio, telefono, id_garage);
    }

    static renderReserva(elSelector, i, reserva_id, horario_transaccion, nombre_garage, direccion, localidad, telefono, estadoReserva){        
        document.querySelector(elSelector).innerHTML += CreadorP.crearReservaDiv(i, reserva_id, horario_transaccion, nombre_garage, direccion, localidad, telefono, estadoReserva);
    }
    
    static renderReservaPorGarage(elSelector, i, fechaReserva, nombreGarage, nombreCliente, apellidoCliente, telefono, mail, garageID, estadoReserva){
        
        document.querySelector(elSelector).innerHTML += CreadorP.crearReservaDivCliente(i, fechaReserva, nombreGarage, nombreCliente, apellidoCliente, telefono, mail, garageID, estadoReserva);
    }

    static crearGarageDiv(i, nombre, direccion, barrio, telefono,id_garage) {
        let salida = "";
        salida = `<div id="garage-div">
            <div id="garage-left">
                <form>
                <div id="garage-datos">`+
                    nombre+`<br>`+
                    direccion+`<br>`+
                    barrio+`<br>`+
                    telefono+`<br>
                </div>
                <div id="_id_garage_`+i+`_" style="visibility:hidden">garage_id=`+id_garage+`</div>
                <div id="garage-reservar"><button type="submit" class="_id_garage_`+i+`_"><div id="reservar-texto">RESERVAR</div><i class="fas fa-car-side"></i></button></div>
                </form>
            </div>
            <div id="garage-img"><img id="logo-medio" src="src/logo.png"></div>
        </div>`;
        return salida;
    }
    
    
    static crearGarageDivCliente(i, nombre, direccion, barrio, telefono, id_garage) {
        let salida = "";
        salida = `<div id="garage-div">
            <div id="garage-left">
                <div id="garage-datos"><h2>`+
                    nombre+`</h2><br>`+
                    direccion+`<br>`+
                    barrio+`<br>`+
                    telefono+`<br>`+`
                </div>
                <div id="_id_garage_`+i+`_" style="display:none">`+id_garage+`</div>
                <div id="garage-reservar"><button type="submit" id="`+i+`" onClick="verReservas(this.id)">VER RESERVAS</div><i class="fas fa-car-side"></i></button></div>
            <div id="garage-img"><img id="logo-medio" src="src/logo.png"></div>
            </div>
        </div>`;
        return salida;
    }

    static crearReservaDiv(i, reserva_id, horario_transaccion, nombre_garage, direccion, localidad, telefono, estadoReserva) {
        let salida = "";
        if (estadoReserva == '1') {
            estadoReserva = "PENDIENTE";
        } else if (estadoReserva == '2') {
            estadoReserva = "<span style='color: green'>APROBADA</span>";
        } else {
            estadoReserva = "<span style='color: red'>CANCELADA</span>";
        }
        salida = `<div id="garage-div">
            <div id="garage-left">
                <form>
                <div id="garage-datos">`+
                    nombre_garage+`<br>`+
                    direccion+`<br>`+
                    localidad+`<br>`+
                    telefono+`<br>`+
                    horario_transaccion+`<br>`+
                    `Estado de Reserva: `+estadoReserva+`<br>
                </div>
                <div id="_id_reserva_`+i+`_" style="visibility:hidden">reserva_id=`+reserva_id+`</div>
                <div id="garage-reservar"><button type="submit" class="_id_reserva_`+i+`_"><div id="reservar-texto">CANCELAR</div><i class="fas fa-car-side"></i></button></div>
                </form>
            </div>
            <div id="garage-img"><img id="logo-medio" src="src/logo.png"></div>
        </div>`;
        return salida;
    }
    
        static crearReservaDivCliente(i, fechaReserva, nombreGarage, nombreCliente, apellidoCliente, telefono, mail, garageID, estadoReserva) {
        let salida = "";
        if (estadoReserva == '1') {
            estadoReserva = "PENDIENTE";
            salida = `<div id="garage-div" style="height:300px !important">
            <div id="garage-left">
                <form>
                    <div id="garage-datos"><h2>Reserva N°`+i+` de `+
                        nombreGarage+`</h2><br>`+
                        `FECHA: `+fechaReserva+`<br>`+
                        `Nombre: `+nombreCliente+`<br>`+
                        `Apellido: `+apellidoCliente+`<br>`+
                        `Telefono: `+telefono+`<br>`+
                        `Mail: `+mail+`<br>`+
                        `Estado de Reserva: `+estadoReserva+`<br>
                    </div>
                    <div id="garage-reservar" style="display: flex">
                        <button type="submit" id="`+i+`" onClick="updateReserva(this.id,'false', `+garageID+`)">CANCELAR<i class="fas fa-car-side"></i></button>
                        <button type="submit" id="`+i+`" onClick="updateReserva(this.id,'true', `+garageID+`)">APROBAR<i class="fas fa-car-side"></i></button>
                    </div>
                </form>
                </div>
                <div id="garage-img"><img id="logo-medio" src="src/logo.png"></div>
            </div>`;
        } else if (estadoReserva == '2') {
            estadoReserva = "<span style='color: green'>APROBADA</span>";
            salida = `<div id="garage-div" style="height:300px !important">
            <div id="garage-left">
                <form>
                    <div id="garage-datos"><h2>Reserva N°`+i+` de `+
                        nombreGarage+`</h2><br>`+
                        `FECHA: `+fechaReserva+`<br>`+
                        `Nombre: `+nombreCliente+`<br>`+
                        `Apellido: `+apellidoCliente+`<br>`+
                        `Telefono: `+telefono+`<br>`+
                        `Mail: `+mail+`<br>`+
                        `Estado de Reserva: `+estadoReserva+`<br>
                    </div>
                    <div id="garage-reservar" style="display: flex">
                        <button type="submit" id="`+i+`" onClick="updateReserva(this.id,'false', `+garageID+`)">CANCELAR<i class="fas fa-car-side"></i></button>
                        
                    </div>
                </form>
                </div>
                <div id="garage-img"><img id="logo-medio" src="src/logo.png"></div>
            </div>`;
        } else {
            estadoReserva = "<span style='color: red'>CANCELADA</span>";
            salida = `<div id="garage-div" style="height:300px !important">
            <div id="garage-left">
                <form>
                    <div id="garage-datos"><h2>Reserva N°`+i+` de `+
                        nombreGarage+`</h2><br>`+
                        `FECHA: `+fechaReserva+`<br>`+
                        `Nombre: `+nombreCliente+`<br>`+
                        `Apellido: `+apellidoCliente+`<br>`+
                        `Telefono: `+telefono+`<br>`+
                        `Mail: `+mail+`<br>`+
                        `Estado de Reserva: `+estadoReserva+`<br>
                    </div>
                    
                    </form>
                </div>
                <div id="garage-img"><img id="logo-medio" src="src/logo.png"></div>
            </div>`;
        }
        return salida;
    }

    }