class Validar {
    static validarCampo(campo, alerta_vacio, alerta_formato, comparacion) {
        let formValue = document.getElementById(campo).value;
        if (formValue == "") {
            document.getElementById(campo + "-error").style.visibility = "visible";
            return alerta_vacio;
        } else {
            document.getElementById(campo + "-error").style.visibility = "hidden";
        }
        if (!comparacion.test(formValue)) {
            document.getElementById(campo + "-error").style.visibility = "visible";
            return alerta_formato;
        } else {
            document.getElementById(campo + "-error").style.visibility = "hidden";
        }
        return "";
    }

    static validarBarrio(campo,alerta_vacio) {
        let sel = document.getElementById(campo);
        let opt;
        for ( let i = 0, len = sel.options.length; i < len; i++ ) {
            opt = sel.options[i];
            if ( opt.selected === true ) {
                break;
            }
        }   
        
        if (opt.value == "default") {
            document.getElementById(campo + "-error").style.visibility = "visible";
            return alerta_vacio;
        } else {
            document.getElementById(campo + "-error").style.visibility = "hidden";
            return "";
        }  
    }

    static validarFormatoPassword(campo) {
        let pass = document.getElementById(campo).value;
        let contenido = /^[a-zA-Z0-9ñÑ!$@.&]+$/;

        if (pass.length < 8 || pass.length > 15 || !contenido.test(pass)) {
            document.getElementById(campo + "-error").style.visibility = "visible";
            return "La contraseña debe tener entre 8 y 15 caracteres, y permite mayúsculas, minúsculas, numeros\n"
                    + "y caracteres especiales ! $ @ . &";
        } else {
            document.getElementById(campo + "-error").style.visibility = "hidden";
            return "";
        }
    }

    static validarRegistroPassword(campo1, campo2) {
        let pass1 = document.getElementById(campo1).value;
        let pass2 = document.getElementById(campo2).value;

        if (pass1 !== pass2) {
                    document.getElementById(campo1 + "-error").style.visibility = "visible";
                    document.getElementById(campo2 + "-error").style.visibility = "visible";
                    return "Las contraseñas deben coincidir\n";
        } else if (pass1.length < 8 || pass2.length < 8 || pass1.length > 15 || pass2.length > 15) {
            document.getElementById(campo1 + "-error").style.visibility = "visible";
            document.getElementById(campo2 + "-error").style.visibility = "visible";
            return "La contraseña debe tener entre 8 y 15 caracteres\n";
        } else {
            let contenido = /^[a-zA-Z0-9ñÑ!$@.&]+$/;
            if (!contenido.test(pass1) || !contenido.test(pass2)) {
                document.getElementById(campo1 + "-error").style.visibility = "visible";
                document.getElementById(campo2 + "-error").style.visibility = "visible";
                return "Las contraseñas permiten letras mayúsculas y minúsculas, números, y caracteres especiales ! $ @ . &";
            }
        }
        document.getElementById(campo1 + "-error").style.visibility = "hidden";
        document.getElementById(campo2 + "-error").style.visibility = "hidden";
        return "";
    }

    static validarForm() {
        let salida_alerta = "";
        //Validar nombre
        salida_alerta += Validar.validarCampo("nombre",
                "Por favor ingrese su nombre\n",
                "El nombre no debe contener números ni caracteres especiales\n",
                /^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/);

        //Validar apellido
        salida_alerta += Validar.validarCampo("apellido",
                "Por favor ingrese su apellido\n",
                "El apellido no debe contener números ni caracteres especiales\n",
                /^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/);

        //Validar correo electronico
        //aca deberia ir chequeo contra base de datos por mail si existe
        salida_alerta += Validar.validarCampo("correoelectronico",
                "Por favor ingrese su correo electrónico\n",
                "Revise el formato de su correo electrónico (usuario@dominio)\n",
                /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i);

        salida_alerta += Validar.validarCampo("telefono",
                "Por favor ingrese su teléfono\n",
                "Revise el formato de su teléfono, sólo debe contener números\n",
                /^[0-9\s]+$/);
                
        //Validar contraseña
        salida_alerta += Validar.validarRegistroPassword("password1", "password2");

        return salida_alerta;
    }

    static validarFormCrearGarage() {
        let salida_alerta = "";
        //Validar nombre del estacionamiento
        salida_alerta += Validar.validarCampo("nombre",
                "Por favor ingrese el nombre del Estacionamiento\n",
                "El nombre del Estacionamiento puede contener letras, números, y caracteres especiales ! $ @ . &\n",
                /^[0-9a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/);

        //Validar dirección del estacionamiento
        salida_alerta += Validar.validarCampo("direccion-garage",
                "Por favor ingrese la dirección del Estacionamiento\n",
                "La dirección del Estacionamiento puede contener letras, números, y caracteres especiales ! $ @ . &\n",
                /^[0-9a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/);

        //Validar barrio del estacionamiento
        salida_alerta += Validar.validarBarrio("filtro-barrio","Por favor seleccione un barrio\n");
        
        //Validar teléfono del estacionamiento
        salida_alerta += Validar.validarCampo("telefono",
                "Por favor ingrese el teléfono del Estacionamiento\n",
                "Revise el formato del teléfono, sólo debe contener números\n",
                /^[0-9\s]+$/);

        return salida_alerta;
    }
    
}