/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test;

import com.guardamelugar.database.GarageDAO;
import com.guardamelugar.database.UsuarioDAO;

/**
 *
 * @author rodrigo.ruiz
 */
public class MainTest {
    
   public static void main(String[] args) {
        System.out.println("[ .. ]TEST USUARIO DAO");
        try {
            String mail = "user@test1.com";
            String contrasena = "usertest1";
            
            boolean existeUsuario = UsuarioDAO.getInstance().validarLogin(mail, contrasena);
            System.out.println("[ .. ]TEST USUARIO DAO EXISTE: " + existeUsuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println("[ OK ]TEST USUARIO DAO");
        
        System.out.println("[ .. ]TEST INSERT");
        try {
            String nombre = "Juan";
            String apellido = "Perez";
            String telefono = "1515151515";
            String correoelectronico = "juan@perez.com.ar";
            String contrasena = "juancito123";
            int rol = 1;         
            boolean existeUsuario = UsuarioDAO.getInstance().existeUsuario(correoelectronico);
            System.out.println("[ .. ]TEST INSERT EXISTE: " + existeUsuario);
            if (!existeUsuario) 
            try {
            UsuarioDAO.getInstance().insertarUsuario(nombre, apellido, telefono, correoelectronico, 
                    contrasena, rol);
            } catch (Exception exc) {
            exc.printStackTrace();
        }
            existeUsuario = UsuarioDAO.getInstance().existeUsuario(correoelectronico);
            System.out.println("[ .. ]TEST INSERT EXISTE: " + existeUsuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println("[ OK ]TEST INSERT");  
        
        System.out.println("[ .. ]TEST GET FILTRO GARAGES");
        try {  
            //GarageDAO.getInstance().DisponibilidadGarages("lugar_autos");
            } catch (Exception exc) {
            exc.printStackTrace();
        }
        System.out.println("[ OK ]TEST GET FILTRO GARAGES OK");
    }
}
