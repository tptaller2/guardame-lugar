SET GLOBAL time_zone = '-3:00';

CREATE DATABASE guardameLugarDB;
USE guardameLugarDB;

CREATE TABLE `roles` ( 
 `rol_id` INT NOT NULL AUTO_INCREMENT , 
 `nombre_rol` VARCHAR(20) NOT NULL , 
 `descripcion` TEXT NOT NULL , 
 PRIMARY KEY (`rol_id`)
) ENGINE = InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `usuarios` (
 `user_id` int(11) NOT NULL AUTO_INCREMENT,
 `nombre` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
 `apellido` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
 `telefono` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
 `mail` varchar(254) COLLATE latin1_spanish_ci NOT NULL,
 `contraseña` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
 `rol` int(11) NOT NULL,
 CONSTRAINT rol_usuario foreign key (rol) references Roles(rol_id),
 PRIMARY KEY (`user_id`),
 UNIQUE KEY `mail` (`mail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `localidades` (
 `localidad_id` int(11) NOT NULL AUTO_INCREMENT,
 `nombre_localidad` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
 PRIMARY KEY (`localidad_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `garages` (
 `garage_id` int(11) NOT NULL AUTO_INCREMENT,
 `nombre_garage` varchar(40) COLLATE latin1_spanish_ci NOT NULL,
 `direccion` varchar(40) COLLATE latin1_spanish_ci NOT NULL,
 `coordenadas` varchar(40) COLLATE latin1_spanish_ci NOT NULL,
 `localidad_garage` int(11) NOT NULL,
 `telefono` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
 `lugar_autos` tinyint(1) NOT NULL,
 `lugar_motos` tinyint(1) NOT NULL,
 `lugar_camionetas` tinyint(1) NOT NULL,
 `lugar_bicicletas` tinyint(1) NOT NULL,
 `altura_maxima` decimal(10,0) NOT NULL,
 CONSTRAINT localidad_del_garage foreign key (localidad_garage) references localidades(localidad_id),
 PRIMARY KEY (`garage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


CREATE TABLE `garage_por_usuario` (
 `garage_usuario_id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) NOT NULL,
 `garage_id` int(11) NOT NULL,
 CONSTRAINT usuario_de_garage_por_usuario foreign key (user_id) references usuarios(user_id),
 CONSTRAINT garage_de_garage_por_usuario foreign key (garage_id) references garages(garage_id),
 PRIMARY KEY (`garage_usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


CREATE TABLE `comentarios_calificacion` (
 `comentario_id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) NOT NULL,
 `garage_id` int(11) NOT NULL,
 `comentario` text COLLATE latin1_spanish_ci NOT NULL,
 `calificacion` int(11) NOT NULL,
 CONSTRAINT usuario_comentario foreign key (user_id) references usuarios(user_id),
 CONSTRAINT garage_comentario foreign key (garage_id) references garages(garage_id),
 PRIMARY KEY (`comentario_id`),
 KEY `user_id` (`user_id`),
 KEY `garage_id` (`garage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


CREATE TABLE `reservas` (
 `reserva_id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) NOT NULL,
 `garage_id` int(11) NOT NULL,
 `horario_transaccion` datetime NOT NULL,
 `activo_sw` tinyint(1) NOT NULL,
 CONSTRAINT usuario_reserva foreign key (user_id) references usuarios(user_id),
 CONSTRAINT garage_reserva foreign key (garage_id) references garages(garage_id),
 PRIMARY KEY (`reserva_id`),
 KEY `user_id` (`user_id`),
 KEY `garage_id` (`garage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

insert into roles values (NULL, 'usuario', 'permite ver los estacionamientos disponibles');
insert into roles values (NULL, 'cliente', 'permite crear estacionamientos propios bajo el usuario, y modificarlos');
insert into localidades values (NULL,'Agronomía');
insert into localidades values (NULL,'Almagro');
insert into localidades values (NULL,'Balvanera');
insert into localidades values (NULL,'Barracas');
insert into localidades values (NULL,'Belgrano');
insert into localidades values (NULL,'Boedo');
insert into localidades values (NULL,'Caballito');
insert into localidades values (NULL,'Chacarita');
insert into localidades values (NULL,'Coghlan');
insert into localidades values (NULL,'Colegiales');
insert into localidades values (NULL,'Constitución');
insert into localidades values (NULL,'Flores');
insert into localidades values (NULL,'Floresta');
insert into localidades values (NULL,'La Boca');
insert into localidades values (NULL,'La Paternal');
insert into localidades values (NULL,'Liniers');
insert into localidades values (NULL,'Mataderos');
insert into localidades values (NULL,'Monte Castro');
insert into localidades values (NULL,'Monserrat');
insert into localidades values (NULL,'Nueva Pompeya');
insert into localidades values (NULL,'Núñez');
insert into localidades values (NULL,'Palermo');
insert into localidades values (NULL,'Parque Avellaneda');
insert into localidades values (NULL,'Parque Chacabuco');
insert into localidades values (NULL,'Parque Chas');
insert into localidades values (NULL,'Parque Patricios');
insert into localidades values (NULL,'Puerto Madero');
insert into localidades values (NULL,'Recoleta');
insert into localidades values (NULL,'Retiro');
insert into localidades values (NULL,'Saavedra');
insert into localidades values (NULL,'San Cristóbal');
insert into localidades values (NULL,'San Nicolás');
insert into localidades values (NULL,'San Telmo');
insert into localidades values (NULL,'Vélez Sársfield');
insert into localidades values (NULL,'Versalles');
insert into localidades values (NULL,'Villa Crespo');
insert into localidades values (NULL,'Villa del Parque');
insert into localidades values (NULL,'Villa Devoto');
insert into localidades values (NULL,'Villa General Mitre');
insert into localidades values (NULL,'Villa Lugano');
insert into localidades values (NULL,'Villa Luro');
insert into localidades values (NULL,'Villa Ortúzar');
insert into localidades values (NULL,'Villa Pueyrredón');
insert into localidades values (NULL,'Villa Real');
insert into localidades values (NULL,'Villa Riachuelo');
insert into localidades values (NULL,'Villa Santa Rita');
insert into localidades values (NULL,'Villa Soldati');
insert into localidades values (NULL,'Villa Urquiza');

insert into usuarios values (NULL, 'User', 'Test1', '1111111111','user@test1.com','KCRMIpWOScIIhVf4YCLpZw==','1');
insert into usuarios values (NULL, 'User', 'Test2', '2222222222','user@test2.com','KCRMIpWOScIIhVf4YCLpZw==','1');
insert into usuarios values (NULL, 'Cliente', 'Test1', '123123123123','cliente@test1.com','KCRMIpWOScIIhVf4YCLpZw==','2');
insert into usuarios values (NULL, 'Cliente', 'Test2', '321321321321','cliente@test2.com','KCRMIpWOScIIhVf4YCLpZw==','2');