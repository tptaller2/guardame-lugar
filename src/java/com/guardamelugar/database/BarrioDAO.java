/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guardamelugar.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Gabriel
 */
public class BarrioDAO {
    
    private BarrioDAO() throws ClassNotFoundException, 
            IOException, SQLException {
    }
    
    private static BarrioDAO INSTANCE = null;
    
    public static BarrioDAO getInstance() throws ClassNotFoundException,
            IOException, SQLException {
        if (INSTANCE == null){
            INSTANCE = new BarrioDAO();
        }
        return INSTANCE;
    }
    
    private final static String SQL_BARRIO_GET = "SELECT * FROM localidades order by 2";
    private final static String SQL_BARRIOSFILTRADOS_GET = "SELECT * FROM localidades "
            + "where localidad_id in (select distinct(localidad_garage) from garages "
            + "where (lugar_autos=1 OR lugar_motos=1 OR lugar_camionetas=1 OR lugar_bicicletas=1)) order by 2";
    
    public String traerBarrios() throws ClassNotFoundException,
            IOException, SQLException {
    
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String lista_barrios = "";
        
        try {
            conn = DB.getInstance().getConnection();
            ps = conn.prepareStatement(SQL_BARRIO_GET);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                int barrio_id = rs.getInt("localidad_id");
                String barrio_nombre = rs.getString("nombre_localidad");
                lista_barrios = lista_barrios + "<option value='"+barrio_id+"'>"+barrio_nombre+"</option>";
            }
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
            try {
                rs.close();
            } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
        }
        return lista_barrios;
        
    }

    public String traerBarriosFiltrados() throws ClassNotFoundException,
            IOException, SQLException {
    
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String lista_barrios = "";
        
        try {
            conn = DB.getInstance().getConnection();
            ps = conn.prepareStatement(SQL_BARRIOSFILTRADOS_GET);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                int barrio_id = rs.getInt("localidad_id");
                String barrio_nombre = rs.getString("nombre_localidad");
                System.out.println(barrio_nombre);
                lista_barrios = lista_barrios + "<option value='"+barrio_id+"'>"+barrio_nombre+"</option>";
            }
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
            try {
                rs.close();
            } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
        }
        return lista_barrios;
        
    }    
    
}
