package com.guardamelugar.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author rgabr
 */
public class DB {
    
    private static DB INSTANCE = null;
    private static String LABASE = "jdbc:mysql://localhost:3306/guardamelugardb"
            + "?useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=America/Buenos_Aires";
    private static String LABASEUSUARIO = "root";  // "root";
    private static String LABASECLAVE = "";    //"root";
    public static DB getInstance() throws ClassNotFoundException, IOException, SQLException {
        if (INSTANCE == null) {
            INSTANCE = new DB();
        }
        return INSTANCE;
    }
    public DB() throws ClassNotFoundException,
            IOException, SQLException {
    }

    public Connection getConnection() throws ClassNotFoundException,
            IOException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection(LABASE, LABASEUSUARIO, LABASECLAVE);
    }

    
}
