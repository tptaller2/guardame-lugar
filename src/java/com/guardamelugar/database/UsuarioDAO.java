/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guardamelugar.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author rgabr
 */
public class UsuarioDAO {
    
    private UsuarioDAO() throws ClassNotFoundException, 
            IOException, SQLException {
    }
    
    private static UsuarioDAO INSTANCE = null;
    
    public static UsuarioDAO getInstance() throws ClassNotFoundException,
            IOException, SQLException {
        if (INSTANCE == null){
            INSTANCE = new UsuarioDAO();
        }
        return INSTANCE;
    }
    
    private final static String SQL_USUARIOS_LOGIN = "SELECT * FROM usuarios " +
            "WHERE mail = ? AND contraseña = ?";
    
    private final static String SQL_USUARIOS_SIGNUP = "INSERT INTO usuarios VALUES (" +
            "NULL, ?, ?, ?, ?, ?, ?)";
    
    private final static String SQL_EXISTE_USUARIO = "SELECT mail from usuarios " +
            "WHERE mail = ?";
    
    final String secretKey = "guardameFAFA";
    
    public boolean validarLogin(String mail, String contrasena) throws ClassNotFoundException,
            IOException, SQLException {
    
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean valido = false;
        
        String encryptedPassword = AES.encrypt(contrasena, secretKey) ;
                
        try {
            conn = DB.getInstance().getConnection();
            ps = conn.prepareStatement(SQL_USUARIOS_LOGIN);
            ps.setString(1, mail);
            ps.setString(2, encryptedPassword);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                valido = true;
            } else {
                valido = false;
            }
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
            try {
                rs.close();
            } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
        }
        return valido;
        
    }

    public String obtenerDatosUsuario(String usuario, String password) throws SQLException {
        
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String nombreUsuario = null;
        String encryptedPassword = AES.encrypt(password, secretKey) ;
        
        try {
            conn = DB.getInstance().getConnection();
            ps = conn.prepareStatement(SQL_USUARIOS_LOGIN);
            ps.setString(1, usuario);
            ps.setString(2, encryptedPassword);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                nombreUsuario = rs.getString("nombre") + " " + rs.getString("apellido");
            }
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
            try {
                rs.close();
            } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
        }
        return nombreUsuario;
    }
    
    public int obtenerRolUsuario(String usuario, String password) throws SQLException {
        
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int rol = 0;
        String encryptedPassword = AES.encrypt(password, secretKey) ;
                
        try {
            conn = DB.getInstance().getConnection();
            ps = conn.prepareStatement(SQL_USUARIOS_LOGIN);
            ps.setString(1, usuario);
            ps.setString(2, encryptedPassword);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                rol = rs.getInt("rol");
            }
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
            try {
                rs.close();
            } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
        }
        return rol;
    }

    public boolean existeUsuario(String mail) throws ClassNotFoundException,
            IOException, SQLException {
    
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;       
        boolean valido = false;
                
        try {
            conn = DB.getInstance().getConnection();
            ps = conn.prepareStatement(SQL_EXISTE_USUARIO);
            ps.setString(1, mail);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                valido = true;
            } else {
                valido = false;
            }
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
            try {
                rs.close();
            } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
        }
        return valido;
    }
    
    public void insertarUsuario(String nombre, String apellido, String telefono, String correoelectronico, 
            String contrasena, int rol) throws SQLException {
        
        Connection conn = null;
        PreparedStatement ps = null;
        
        String encryptedPassword = AES.encrypt(contrasena, secretKey) ;
        
        try {
            conn = DB.getInstance().getConnection();
            ps = conn.prepareStatement(SQL_USUARIOS_SIGNUP);
            ps.setString(1, nombre);
            ps.setString(2, apellido);
            ps.setString(3, telefono);
            ps.setString(4, correoelectronico);
            ps.setString(5, encryptedPassword);
            ps.setInt(6, rol);
            ps.executeUpdate();

        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
    }
    
}
