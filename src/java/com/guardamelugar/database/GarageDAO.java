/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guardamelugar.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Leandro
 */
public class GarageDAO {
    
    private GarageDAO() throws ClassNotFoundException, 
            IOException, SQLException {
    }
    
    private static GarageDAO INSTANCE = null;
    
    public static GarageDAO getInstance() throws ClassNotFoundException,
            IOException, SQLException {
        if (INSTANCE == null){
            INSTANCE = new GarageDAO();
        }
        return INSTANCE;
    }
    
    private final static String SQL_TRAER_TODOS_GARAGES = "SELECT * FROM garages INNER JOIN "
            + "localidades on localidad_garage = localidad_id;";

    private final static String SQL_DISP_GARAGES_PROPIOS = "SELECT * FROM garages INNER JOIN "
            + "localidades on localidad_garage = localidad_id "
            + "INNER JOIN garage_por_usuario on garage_por_usuario.garage_id = garages.garage_id "
            + "INNER JOIN usuarios on garage_por_usuario.user_id = usuarios.user_id "
            + "where usuarios.mail = ?";
    
    private final static String SQL_DISP_GARAGES_AUTOS = "SELECT * FROM garages INNER JOIN "
            + "localidades on localidad_garage = localidad_id where lugar_autos = '1' "
            + "and localidad_id= ? limit 10";
    
    private final static String SQL_DISP_GARAGES_CAMIONETAS = "SELECT * FROM garages INNER JOIN "
            + "localidades on localidad_garage = localidad_id where lugar_camionetas = '1' "
            + "and localidad_id= ? limit 10";
    
    private final static String SQL_DISP_GARAGES_MOTOS = "SELECT * FROM garages INNER JOIN "
            + "localidades on localidad_garage = localidad_id where lugar_motos = '1' "
            + "and localidad_id= ? limit 10";
    
    private final static String SQL_DISP_GARAGES_BICIS = "SELECT * FROM garages INNER JOIN "
            + "localidades on localidad_garage = localidad_id where lugar_bicicletas = '1' "
            + "and localidad_id= ? limit 10";
    
    private final static String SQL_GRABAR_GARAGE = "INSERT INTO GARAGES VALUES "
            + "(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    private final static String SQL_GRABAR_GARAGE_USUARIO = "INSERT INTO GARAGE_POR_USUARIO VALUES "
            + "(NULL, (select user_id from usuarios where mail= ?), ?)";
    
    public String DisponibilidadGarages(String seleccion, String barrio) throws ClassNotFoundException,
            IOException, SQLException {
    
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String lista_garages = "[";
        
        try {
            conn = DB.getInstance().getConnection();
            if (seleccion.matches("lugar_autos")) {
            ps = conn.prepareStatement(SQL_DISP_GARAGES_AUTOS);                
            } else {
                if (seleccion.matches("lugar_camionetas")) {
                    ps = conn.prepareStatement(SQL_DISP_GARAGES_CAMIONETAS);
                } else {
                    if (seleccion.matches("lugar_motos")) {
                        ps = conn.prepareStatement(SQL_DISP_GARAGES_MOTOS);
                    } else {
                        ps = conn.prepareStatement(SQL_DISP_GARAGES_BICIS);
                    }
                }
            }
            ps.setString(1, barrio);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                int garage_id = rs.getInt("garage_id");
                String nombre_garage = rs.getString("nombre_garage");
                String direccion = rs.getString("direccion");
                String telefono = rs.getString("telefono");
                int lugar_autos = rs.getInt("lugar_autos");
                int lugar_motos = rs.getInt("lugar_motos");
                int lugar_camionetas = rs.getInt("lugar_camionetas");
                int lugar_bicicletas = rs.getInt("lugar_bicicletas");
                double altura_maxima = rs.getDouble("altura_maxima");
                String nombre_localidad = rs.getString("nombre_localidad");
                String barrio_nombre = rs.getString("nombre_localidad");
                lista_garages = lista_garages + "{\"nombre\": " + "\"" + nombre_garage + "\", " + 
                        "\"direccion\": " + "\"" + direccion + "\", " +
                        "\"telefono\": " + "\"" + telefono + "\", " +
                        "\"barrio\": " + "\"" + barrio_nombre + "\"," +
                        "\"id_garage\": \"" + garage_id + "\"},";
            }
            
            
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
            try {
                rs.close();
            } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
        }
        lista_garages = lista_garages.substring(0, lista_garages.length() - 1) + "]";
        return lista_garages;
        
    }

    public String DisponibilidadGarages(String usuario) throws ClassNotFoundException,
            IOException, SQLException {
    
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String lista_garages = "[";
        
        try {
            conn = DB.getInstance().getConnection();
            ps = conn.prepareStatement(SQL_DISP_GARAGES_PROPIOS);                
            
            ps.setString(1, usuario);
            
            rs = ps.executeQuery();
                        
            while (rs.next()) {
                int garage_id = rs.getInt("garage_id");
                String nombre_garage = rs.getString("nombre_garage");
                String direccion = rs.getString("direccion");
                String telefono = rs.getString("telefono");
                int lugar_autos = rs.getInt("lugar_autos");
                int lugar_motos = rs.getInt("lugar_motos");
                int lugar_camionetas = rs.getInt("lugar_camionetas");
                int lugar_bicicletas = rs.getInt("lugar_bicicletas");
                double altura_maxima = rs.getDouble("altura_maxima");
                String nombre_localidad = rs.getString("nombre_localidad");
                String barrio_nombre = rs.getString("nombre_localidad");
                lista_garages = lista_garages + "{\"id_garage\": \"" + garage_id + "\"," + 
                        "\"nombre\": " + "\"" + nombre_garage + "\", " + 
                        "\"direccion\": " + "\"" + direccion + "\", " +
                        "\"telefono\": " + "\"" + telefono + "\", " +
                        "\"barrio\": " + "\"" + barrio_nombre + "\"},";
            }
            
            
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
            try {
                rs.close();
            } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
        }
        lista_garages = lista_garages.substring(0, lista_garages.length() - 1) + "]";
        return lista_garages;
        
    }
    
    public void GuardameGarage(String usuario, String nombre, String direccion, String barrio, String telefono,
            String autos, String motos, String camionetas, String bicicletas) throws SQLException {
        
        Connection conn = null;
        PreparedStatement ps = null;
        PreparedStatement pss = null;
                
        try {
            conn = DB.getInstance().getConnection();
            String carga_autos = "";
            String carga_motos = "";
            String carga_camionetas = "";
            String carga_bicicletas = "";
            ps = conn.prepareStatement(SQL_GRABAR_GARAGE, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, nombre);
            ps.setString(2, direccion);
            ps.setInt(3, 0);
            ps.setString(4, barrio);
            ps.setString(5, telefono);
            if (autos.equals("on")) {
                carga_autos = "1";
            } else if (autos.equals("false")) {
                carga_autos = "0";
            }
            if (motos.equals("on")) {
                carga_motos = "1";
            } else if (motos.equals("false")) {
                carga_motos = "0";
            }
            if (camionetas.equals("on")) {
                carga_camionetas = "1";
            } else if (camionetas.equals("false")) {
                carga_camionetas = "0";
            }
            if (bicicletas.equals("on")) {
                carga_bicicletas = "1";
            } else if (bicicletas.equals("false")) {
                carga_bicicletas = "0";
            }
            ps.setString(6, carga_autos);
            ps.setString(7, carga_motos);
            ps.setString(8, carga_camionetas);
            ps.setString(9, carga_bicicletas);
            ps.setInt(10, 0);
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            int auto_id = rs.getInt(1);
            
            pss = conn.prepareStatement(SQL_GRABAR_GARAGE_USUARIO);
            pss.setString(1, usuario);
            pss.setInt(2, auto_id);
            
            pss.executeUpdate();
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
    }

    public String traerGarages() throws SQLException {
        
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String lista_garages = "[";
        
        try {
            conn = DB.getInstance().getConnection();
            ps = conn.prepareStatement(SQL_TRAER_TODOS_GARAGES);                
            
            rs = ps.executeQuery();
                        
            while (rs.next()) {
                int garage_id = rs.getInt("garage_id");
                String nombre_garage = rs.getString("nombre_garage");
                String direccion = rs.getString("direccion");
                String telefono = rs.getString("telefono");
                
                String barrio_nombre = rs.getString("nombre_localidad");
                lista_garages = lista_garages + "{\"id_garage\": \"" + garage_id + "\"," + 
                        "\"nombre\": " + "\"" + nombre_garage + "\", " + 
                        "\"direccion\": " + "\"" + direccion + "\", " +
                        "\"telefono\": " + "\"" + telefono + "\", " +
                        "\"barrio\": " + "\"" + barrio_nombre + "\"},";
            }
            
            
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
            try {
                rs.close();
            } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
        }
        lista_garages = lista_garages.substring(0, lista_garages.length() - 1) + "]";
        return lista_garages;
        
    }
        
}