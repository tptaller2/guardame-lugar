/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guardamelugar.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Gabriel
 */
public class ReservaDAO {
    
    private ReservaDAO() throws ClassNotFoundException, 
            IOException, SQLException {
    }
    
    private static ReservaDAO INSTANCE = null;
    
    public static ReservaDAO getInstance() throws ClassNotFoundException,
            IOException, SQLException {
        if (INSTANCE == null){
            INSTANCE = new ReservaDAO();
        }
        return INSTANCE;
    }

    private final static String SQL_RESERVAR_GARAGE = "INSERT INTO RESERVAS VALUES "
            + "(NULL, (select user_id from usuarios where mail = ?), ?, (select SYSDATE()), 1)";

    private final static String SQL_VER_RESERVAS = "SELECT RESERVA_ID, R.HORARIO_TRANSACCION, R.ACTIVO_SW, "
            + "G.NOMBRE_GARAGE, G.DIRECCION, L.NOMBRE_LOCALIDAD, G.TELEFONO "
            + "FROM RESERVAS R INNER JOIN GARAGES G ON R.GARAGE_ID = G.GARAGE_ID INNER JOIN LOCALIDADES L "
            + "ON L.LOCALIDAD_ID = G.LOCALIDAD_GARAGE "
            + "WHERE R.USER_ID = (SELECT USER_ID FROM USUARIOS WHERE MAIL = ?);";
            
    
    private final static String SQL_VER_RESERVAS_GARAGE = "SELECT RESERVA_ID, R.HORARIO_TRANSACCION, R.ACTIVO_SW, "
            + "U.NOMBRE, U.APELLIDO, U.TELEFONO, U.MAIL , G.NOMBRE_GARAGE, G.GARAGE_ID "
            + "FROM RESERVAS R INNER JOIN USUARIOS U ON R.USER_ID = U.USER_ID INNER JOIN GARAGES G ON R.GARAGE_ID = G.GARAGE_ID "
            + "WHERE R.garage_ID = ?;";
    
    private final static String SQL_CANCELAR_RESERVA = "UPDATE RESERVAS SET ACTIVO_SW = 0 "
            + "WHERE RESERVA_ID = ? AND USER_ID = (SELECT USER_ID FROM USUARIOS WHERE MAIL = ?);";
    
    private final static String SQL_UPDATE_RESERVA_CLIENTE = "UPDATE RESERVAS SET ACTIVO_SW = ? "
            + "WHERE RESERVA_ID = ?;";
    
    public void ReservarGarage(String usuario, int garage_id) throws SQLException {
        
        Connection conn = null;
        PreparedStatement ps = null;
        
        try {
            conn = DB.getInstance().getConnection();
            ps = conn.prepareStatement(SQL_RESERVAR_GARAGE);
            ps.setString(1, usuario);
            ps.setInt(2, garage_id);
            
            ps.executeUpdate();
             
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
    }
    
    public void CancelarReserva(int reserva_id, String usuario) throws SQLException {
        
        Connection conn = null;
        PreparedStatement ps = null;
        
        try {
            conn = DB.getInstance().getConnection();
            ps = conn.prepareStatement(SQL_CANCELAR_RESERVA);
            ps.setInt(1, reserva_id);
            ps.setString(2, usuario);
            
            ps.executeUpdate();
             
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
    }

    public String VerReservas(String usuario) throws SQLException {
        
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String lista_reservas = "[";
        
        try {
            conn = DB.getInstance().getConnection();
            ps = conn.prepareStatement(SQL_VER_RESERVAS);
            ps.setString(1, usuario);
            
            rs = ps.executeQuery();
            
            while (rs.next()) {
                int reserva_id = rs.getInt("reserva_id");
                Date horario_transaccion = rs.getDate("horario_transaccion");
                String nombre_garage = rs.getString("nombre_garage");
                String direccion = rs.getString("direccion");
                String localidad = rs.getString("nombre_localidad");
                String telefono = rs.getString("telefono");
                String estadoReserva = rs.getString("activo_sw");
                lista_reservas = lista_reservas + "{\"reserva_id\": \"" + reserva_id + "\"," + 
                        "\"horario_transaccion\": " + "\"" + horario_transaccion + "\", " +
                        "\"nombre_garage\": " + "\"" + nombre_garage + "\", " +
                        "\"direccion\": " + "\"" + direccion + "\", " +
                        "\"localidad\": " + "\"" + localidad + "\", " +
                        "\"telefono\": " + "\"" + telefono + "\", " +
                        "\"estadoReserva\": " + "\"" + estadoReserva + "\"},";
            }
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
        if (lista_reservas.length() > 1) {
            lista_reservas = lista_reservas.substring(0, lista_reservas.length() - 1) + "]";
        }
        else {
            lista_reservas = lista_reservas + "]";
        }
        System.out.println(lista_reservas);
        return lista_reservas;
    }
    
    public String verReservasPorGarage(String garageID) throws SQLException {
        
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String lista_reservas = "[";
        
        try {
            conn = DB.getInstance().getConnection();
            ps = conn.prepareStatement(SQL_VER_RESERVAS_GARAGE);
            ps.setString(1, garageID);
            
            rs = ps.executeQuery();
            
            while (rs.next()) {
                int reserva_id = rs.getInt("reserva_id");
                Date horario_transaccion = rs.getDate("horario_transaccion");
                String nombre_garage = rs.getString("nombre_garage");
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                String telefono = rs.getString("telefono");
                String mail = rs.getString("mail");
                String estadoReserva = rs.getString("activo_sw");
                lista_reservas = lista_reservas + "{\"reserva_id\": \"" + reserva_id + "\"," + 
                        "\"horario_transaccion\": " + "\"" + horario_transaccion + "\", " +
                        "\"nombre_garage\": " + "\"" + nombre_garage + "\", " +
                        "\"nombre\": " + "\"" + nombre + "\", " +
                        "\"apellido\": " + "\"" + apellido + "\", " +
                        "\"telefono\": " + "\"" + telefono + "\", " +
                        "\"mail\": " + "\"" + mail + "\", " +
                        "\"garageID\": " + "\"" + garageID + "\", " +
                        "\"estadoReserva\": " + "\"" + estadoReserva + "\"},";
            }
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
        if (lista_reservas.length() > 1) {
            lista_reservas = lista_reservas.substring(0, lista_reservas.length() - 1) + "]";
        }
        else {
            lista_reservas = "undefined";
        }
        System.out.println(lista_reservas);
        return lista_reservas;
    }
    
    public void cancelarReservaCliente(String reserva_id) throws SQLException {
        
        Connection conn = null;
        PreparedStatement ps = null;
        
        try {
            conn = DB.getInstance().getConnection();
            ps = conn.prepareStatement(SQL_UPDATE_RESERVA_CLIENTE);
            ps.setInt(1, 0);
            ps.setString(2, reserva_id);
            
            ps.executeUpdate();
             
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
    }
    
    public void aprobarReservaCliente(String reserva_id) throws SQLException {
        
        Connection conn = null;
        PreparedStatement ps = null;
        
        try {
            conn = DB.getInstance().getConnection();
            ps = conn.prepareStatement(SQL_UPDATE_RESERVA_CLIENTE);
            ps.setInt(1, 2);
            ps.setString(2, reserva_id);
            
            ps.executeUpdate();
             
        } catch (Exception e) {
            
            e.printStackTrace();
            
        } finally {
                try {
                    ps.close();
                } finally {
                    conn.close();
                }
            }
    }

}