/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guardamelugar.servlets;

import com.guardamelugar.database.GarageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rgabr
 */
public class CargarGarages extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html; charset=utf-8");
        PrintWriter out = response.getWriter();
        String salida = "";
        try {
            salida = GarageDAO.getInstance().traerGarages();
        } catch (ClassNotFoundException ex) {
           Logger.getLogger(CargarBarrios.class.getName()).log(Level.SEVERE, null, ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            out.println("En este momento el sistema no puede conectar con la base de datos, "
                    + "por favor intentelo más tarde");
        } catch (SQLException ex) {
            Logger.getLogger(CargarGarages.class.getName()).log(Level.SEVERE, null, ex);
        }
        out.println(salida);
    }

    
    @Override
    public String getServletInfo() {
        return "Servlet que trae todos los garages";
    }// </editor-fold>

}
