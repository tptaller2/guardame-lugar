/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guardamelugar.servlets;

import com.guardamelugar.database.ReservaDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Leandro
 */
public class ObtenerDatosReservas extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    PrintWriter out = response.getWriter();
    try {
        response.setContentType("text/html; charset=utf-8");
        
    
        String garageID = request.getParameter("garageID");
        String salida = ReservaDAO.getInstance().verReservasPorGarage(garageID);
        
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");
        
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");
        out.print(salida);
        out.close();
        
    } catch (ClassNotFoundException ex) {
        Logger.getLogger(CargarBarrios.class.getName()).log(Level.SEVERE, null, ex);
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        out.println("En este momento el sistema no puede conectar con la base de datos, "
            + "por favor intentelo más tarde");
        out.close();
    } catch (SQLException ex) {
        Logger.getLogger(ObtenerDatosReservas.class.getName()).log(Level.SEVERE, null, ex);
        out.close();
    }

} 

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet destinado a controlar el logueo de usuarios";
    }// </editor-fold>

}