/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guardamelugar.servlets;

import com.guardamelugar.database.UsuarioDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Gabriel
 */
public class Signup extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        try {
            
            String nombre = request.getParameter("nombre");
            String apellido = request.getParameter("apellido");
            String telefono = request.getParameter("telefono");
            String correoelectronico = request.getParameter("correoelectronico");
            String password1 = request.getParameter("password1");
            int rol = Integer.parseInt(request.getParameter("rol"));
            String nombreUsuario = nombre + " " + apellido;
            
            boolean existeUsuario = UsuarioDAO.getInstance().existeUsuario(correoelectronico);
            
            if (existeUsuario == false) {
                UsuarioDAO.getInstance().insertarUsuario(nombre, apellido, telefono, correoelectronico, 
                        password1, rol);
                response.setStatus(HttpServletResponse.SC_OK);
                response.setContentType("text/xml");
                out.append("Gracias por registrarte en Guardame Lugar, " + nombreUsuario);
            } else {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                response.setContentType("text/xml");
                out.append("El correo electrónico ingresado ya existe en nuestro sistema. Si olvidaste tu contraseña podés resetearla.");
            }
                       
        } catch (ClassNotFoundException ex) {
            out.println("Verificar: " + ex.getMessage());
        } catch (SQLException ex) {
            out.println("Verificar:" + ex.getMessage());
        } catch (Exception ex) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            out.println("En este momento el sistema no puede conectar con la base de datos, "
                    + "por favor intentelo más tarde");
        } finally {
            out.close();
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet destinado a controlar el registro de usuarios";
    }// </editor-fold>

}
