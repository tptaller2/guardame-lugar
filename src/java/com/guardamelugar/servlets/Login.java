/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guardamelugar.servlets;

import com.guardamelugar.database.UsuarioDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rgabr
 */
public class Login extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        try {
            
            String usuario = request.getParameter("usuario");
            String password = request.getParameter("contrasena");
            String nombreUsuario = null;
            int rol;
            
            boolean existeUsuario = UsuarioDAO.getInstance().validarLogin(usuario, password);
            
            if (existeUsuario == true) {
                nombreUsuario = UsuarioDAO.getInstance().obtenerDatosUsuario(usuario, password);
                response.setContentType("text/xml");
                rol = UsuarioDAO.getInstance().obtenerRolUsuario(usuario, password);
                if (rol == 1) {
                    response.setStatus(HttpServletResponse.SC_OK);
                } else if (rol == 2) {
                    response.setStatus(HttpServletResponse.SC_ACCEPTED);
                }
                out.append("Bienvenido " + nombreUsuario + " a Guardame Lugar");
            } else {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                response.setContentType("text/xml");
                out.append("El usuario o la password ingresados son incorrectos. Por favor revise el texto ingresado");
            }
                       
        } catch (ClassNotFoundException ex) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            out.println("En este momento el sistema no puede conectar con la base de datos, "
                    + "por favor intentelo más tarde");
        } catch (SQLException ex) {
            out.println("Verificar:" + ex.getMessage());
        } catch (Exception ex) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            out.println("En este momento el sistema no puede conectar con la base de datos, "
                    + "por favor intentelo más tarde");
        } finally {
            out.close();
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet destinado a controlar el logueo de usuarios";
    }// </editor-fold>

}
