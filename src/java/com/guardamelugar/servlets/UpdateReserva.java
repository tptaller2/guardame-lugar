/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guardamelugar.servlets;

import com.guardamelugar.database.ReservaDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rgabr
 */
public class UpdateReserva extends HttpServlet {

    

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        String reservaID = request.getParameter("reservaID");
        String updateCond = request.getParameter("update");
        
        try {
            if (updateCond.equalsIgnoreCase("false")) {
            ReservaDAO.getInstance().cancelarReservaCliente(reservaID);
            } else if (updateCond.equalsIgnoreCase("true")) {
                ReservaDAO.getInstance().aprobarReservaCliente(reservaID);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CargarBarrios.class.getName()).log(Level.SEVERE, null, ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            out.println("En este momento el sistema no puede conectar con la base de datos, "
                + "por favor intentelo más tarde");
            out.close();
        } catch (SQLException ex) {
            Logger.getLogger(ObtenerDatosReservas.class.getName()).log(Level.SEVERE, null, ex);
            out.close();
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet destinado a controlar update de las reservas en el sistema";
    }// </editor-fold>

}
