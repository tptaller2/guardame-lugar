/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guardamelugar.servlets;

import com.guardamelugar.database.GarageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ObtenerDatosMisGarages extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html; charset=utf-8");
        PrintWriter out = response.getWriter();
    try {
        String usuario = request.getParameter("nombredeusuario");
        String salida = GarageDAO.getInstance().DisponibilidadGarages(usuario);
        
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");
        out.println(salida);
        
    } catch (ClassNotFoundException ex) {
        Logger.getLogger(CargarBarrios.class.getName()).log(Level.SEVERE, null, ex);
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            out.println("En este momento el sistema no puede conectar con la base de datos, "
                    + "por favor intentelo más tarde");
    } catch (SQLException ex) {
        Logger.getLogger(CargarBarrios.class.getName()).log(Level.SEVERE, null, ex);
    }

} 

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet destinado a cargar Garages propios";
    }// </editor-fold>

}