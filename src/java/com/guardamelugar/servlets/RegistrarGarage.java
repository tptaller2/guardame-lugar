/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guardamelugar.servlets;

import com.guardamelugar.database.GarageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Gabriel
 */
public class RegistrarGarage extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        try {

            String nombre = request.getParameter("nombre");
            String direccion = request.getParameter("direccion-garage");
            String barrio = request.getParameter("filtro-barrio");
            String telefono = request.getParameter("telefono");
            String autos = request.getParameter("lugar_autos");
            String motos = request.getParameter("lugar_motos");
            String camionetas = request.getParameter("lugar_camionetas");
            String bicicletas = request.getParameter("lugar_bicicletas");
            String usuario = request.getParameter("nombredeusuario");
                 
            GarageDAO.getInstance().GuardameGarage(usuario, nombre, direccion, barrio, telefono, autos, motos, camionetas, bicicletas);

            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType("text/xml");
            out.append("El garage fue guardado!");
            } catch (Exception ex) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            out.println("En este momento el sistema no puede conectar con la base de datos, "
                    + "por favor intentelo más tarde");
        } finally {
            out.close();
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet destinado a guardar Garages";
    }// </editor-fold>

}